<?php

namespace App\Console\Commands;

use App\Models\Discount;
use App\Models\PriceList;
use Illuminate\Console\Command;

class checkout extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'checkout {code} {status_customer}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $code = $this->argument('code');
        $status_customer = $this->argument('status_customer');

        $codes = explode(' ', $code);
        $price = 0;
        $product_price = [];
        $product_name = [];

        foreach ($codes as $code) {
            $product = PriceList::where('code', $code)->first();
            $product_price[] = $product->price;
            $product_name[] = $product->product_name;
            $price = $price + $product->price;
        }
        $quantity_menu = sizeof($codes);

        // discount karyawan
        if ($status_customer == 'employee') {
            $discount = 'Discount Karyawan';
            $discount = Discount::where('discount_name', $discount)->first();
            $total_price = $price - ($price * $discount->discount_value / 100);
            $list_order = array_count_values($product_name);
            // dd($product_name);
            for ($i = 0; $i < 1; $i++) {
                foreach (array_unique($product_name) as $pn) {
                    print_r($pn . " " . $list_order[$pn] . "|");
                }
                print_r('discount: ' . $discount->discount_value . "|" . " " . 'Total :' . 'IDR ' . number_format($total_price, 0, ',', '.'));
            }
        }

        // no discount
        if ($quantity_menu == 1 && $status_customer == 'customer') {
            $total_price = $price;
            $list_order = array_count_values($product_name);
            for ($i = 0; $i < $quantity_menu; $i++) {
                foreach (array_unique($product_name) as $pn) {
                    print_r($pn . " " . $list_order[$pn] . "|");
                }
                print_r('IDR ' . number_format($total_price, 0, ',', '.'));
            }
        }

        // discount buy 1 get 1
        if ($quantity_menu > 1 && $quantity_menu <= 3 && $status_customer == 'customer') {
            $discount = 'Discount Buy 1 Get 1';
            $discount = Discount::where('discount_name', $discount)->first();
            $total_price = $price - min($product_price);
            $list_order = array_count_values($product_name);
            // dd($product_name);
            for ($i = 0; $i < 1; $i++) {
                foreach (array_unique($product_name) as $pn) {
                    print_r($pn . " " . $list_order[$pn] . "|");
                }
                print_r('discount: ' . $discount->discount_value . "|" . " " . 'Total :' . 'IDR ' . number_format($total_price, 0, ',', '.'));
            }
        }

        // $this->info('discount: ' . $discount->discount_value . "|" . " " . 'Total :' . 'IDR ' . number_format($total_price, 0, ',', '.'));
    }
}
