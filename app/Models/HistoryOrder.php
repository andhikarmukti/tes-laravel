<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HistoryOrder extends Model
{
    use HasFactory;
    protected $guarded = ['id'];

    public function discount()
    {
        return $this->belongsTo(Discount::class, 'discount_id');
    }
}
