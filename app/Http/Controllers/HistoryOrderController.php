<?php

namespace App\Http\Controllers;

use App\Models\PriceList;
use App\Models\HistoryOrder;
use Illuminate\Http\Request;
use App\Http\Requests\StoreHistoryOrderRequest;
use App\Http\Requests\UpdateHistoryOrderRequest;
use App\Models\Discount;

class HistoryOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pricelists = PriceList::all();

        return view('history-order.index', compact(
            'pricelists'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreHistoryOrderRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $codes = explode(' ', $request->code);
        $price = 0;
        $product_price = [];
        $product_name = [];

        foreach ($codes as $code) {
            $product = PriceList::where('code', $code)->first();
            $product_price[] = $product->price;
            $product_name[] = $product->product_name;
            $price = $price + $product->price;
        }
        $quantity_menu = sizeof($codes);

        // discount karyawan
        if ($request->status_customer == 'employee') {
            $discount = 'Discount Karyawan';
            $discount = Discount::where('discount_name', $discount)->first();
            $total_price = $price - ($price * $discount->discount_value / 100);
            $list_order = array_count_values($product_name);
            // dd($product_name);
            for ($i = 0; $i < 1; $i++) {
                foreach (array_unique($product_name) as $pn) {
                    print_r($pn . " " . $list_order[$pn] . "|");
                }
                print_r('discount: ' . $discount->discount_value . "|" . " " . 'Total :' . 'IDR ' . number_format($total_price, 0, ',', '.'));
            }
        }

        // no discount
        if ($quantity_menu == 1 && $request->status_customer == 'customer') {
            $total_price = $price;
            $list_order = array_count_values($product_name);
            for ($i = 0; $i < $quantity_menu; $i++) {
                foreach (array_unique($product_name) as $pn) {
                    print_r($pn . " " . $list_order[$pn] . "|");
                }
                print_r('IDR ' . number_format($total_price, 0, ',', '.'));
            }
        }

        // discount buy 1 get 1
        if ($quantity_menu > 1 && $quantity_menu <= 3 && $request->status_customer == 'customer') {
            $discount = 'Discount Buy 1 Get 1';
            $discount = Discount::where('discount_name', $discount)->first();
            $total_price = $price - min($product_price);
            $list_order = array_count_values($product_name);
            // dd($product_name);
            for ($i = 0; $i < 1; $i++) {
                foreach (array_unique($product_name) as $pn) {
                    print_r($pn . " " . $list_order[$pn] . "|");
                }
                print_r('discount: ' . $discount->discount_value . "|" . " " . 'Total :' . 'IDR ' . number_format($total_price, 0, ',', '.'));
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\HistoryOrder  $historyOrder
     * @return \Illuminate\Http\Response
     */
    public function show(HistoryOrder $historyOrder)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\HistoryOrder  $historyOrder
     * @return \Illuminate\Http\Response
     */
    public function edit(HistoryOrder $historyOrder)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateHistoryOrderRequest  $request
     * @param  \App\Models\HistoryOrder  $historyOrder
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateHistoryOrderRequest $request, HistoryOrder $historyOrder)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\HistoryOrder  $historyOrder
     * @return \Illuminate\Http\Response
     */
    public function destroy(HistoryOrder $historyOrder)
    {
        //
    }
}
