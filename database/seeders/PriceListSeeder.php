<?php

namespace Database\Seeders;

use App\Models\PriceList;
use Illuminate\Database\Seeder;

class PriceListSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PriceList::create([
            'code' => 'FR1',
            'product_name' => 'Frappucino',
            'area_id' => 1,
            'price' => 40000
        ]);
        PriceList::create([
            'code' => 'CL1',
            'product_name' => 'Caffe Latte',
            'area_id' => 1,
            'price' => 30000
        ]);
        PriceList::create([
            'code' => 'BC1',
            'product_name' => 'Black Coffee',
            'area_id' => 1,
            'price' => 20000
        ]);
        PriceList::create([
            'code' => 'KL1',
            'product_name' => 'Kopi Luwak',
            'area_id' => 1,
            'price' => 50000
        ]);

        PriceList::create([
            'code' => 'FR2',
            'product_name' => 'Frappucino',
            'area_id' => 2,
            'price' => 50000
        ]);
        PriceList::create([
            'code' => 'CL2',
            'product_name' => 'Caffe Latte',
            'area_id' => 2,
            'price' => 40000
        ]);
        PriceList::create([
            'code' => 'BC2',
            'product_name' => 'Black Coffee',
            'area_id' => 2,
            'price' => 30000
        ]);
        PriceList::create([
            'code' => 'KL2',
            'product_name' => 'Kopi Luwak',
            'area_id' => 2,
            'price' => 80000
        ]);

        PriceList::create([
            'code' => 'FR3',
            'product_name' => 'Frappucino',
            'area_id' => 3,
            'price' => 30000
        ]);
        PriceList::create([
            'code' => 'CL3',
            'product_name' => 'Caffe Latte',
            'area_id' => 3,
            'price' => 20000
        ]);
        PriceList::create([
            'code' => 'BC3',
            'product_name' => 'Black Coffee',
            'area_id' => 3,
            'price' => 10000
        ]);
        PriceList::create([
            'code' => 'KL3',
            'product_name' => 'Kopi Luwak',
            'area_id' => 3,
            'price' => 40000
        ]);

        PriceList::create([
            'code' => 'FR4',
            'product_name' => 'Frappucino',
            'area_id' => 4,
            'price' => 40000
        ]);
        PriceList::create([
            'code' => 'CL4',
            'product_name' => 'Caffe Latte',
            'area_id' => 4,
            'price' => 20000
        ]);
        PriceList::create([
            'code' => 'BC4',
            'product_name' => 'Black Coffee',
            'area_id' => 4,
            'price' => 10000
        ]);
        PriceList::create([
            'code' => 'KL4',
            'product_name' => 'Kopi Luwak',
            'area_id' => 4,
            'price' => 60000
        ]);
    }
}
