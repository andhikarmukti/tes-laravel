<?php

namespace Database\Seeders;

use App\Models\Discount;
use Illuminate\Database\Seeder;

class DiscountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Discount::create([
            'discount_name' => 'Discount Karyawan',
            'discount_value' => 15,
        ]);
        Discount::create([
            'discount_name' => 'Discount Buy 3',
            'discount_value' => 20,
        ]);
        Discount::create([
            'discount_name' => 'Discount Buy 1 Get 1',
            'discount_value' => 'free 1',
        ]);
    }
}
