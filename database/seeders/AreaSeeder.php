<?php

namespace Database\Seeders;

use App\Models\Area;
use Illuminate\Database\Seeder;

class AreaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Area::create([
            'area_name' => 'P. Jawa'
        ]);
        Area::create([
            'area_name' => 'Bali'
        ]);
        Area::create([
            'area_name' => 'P. Sumatra'
        ]);
        Area::create([
            'area_name' => 'P. Kalimantan'
        ]);
    }
}
