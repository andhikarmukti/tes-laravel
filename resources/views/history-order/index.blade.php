@extends('layouts.main')

@section('content')
<h3>Buy Item</h3>
<div class="col-lg-6">
    <form action="/order" method="post">
        @csrf
        {{-- <div class="col d-flex">
            <div class="mb-3 col-6">
              <label for="code1" class="form-label">Product</label>
              <select class="form-select" id="code1" name="code1">
                <option selected disabled>--</option>
                @foreach ($pricelists as $pricelist)
                <option value="{{ $pricelist->code }}">{{ $pricelist->code }}</option>
                @endforeach
              </select>
            </div>
            <div class="mb-3 col-2">
              <label for="quantity1" class="form-label">Quantity</label>
              <input type="number" class="form-control" id="quantity1" name="quantity1">
            </div>
        </div> --}}
        {{-- <div class="mb-5">
            <a href="#">add more..</a>
        </div> --}}
        <div class="col-4 mb-4">
            <input type="text" class="form-control mb-4" name="code">
            <select class="form-select" name="status_customer">
                <option value="customer" selected>Customer</option>
                <option value="employee">Employee</option>
            </select>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
      </form>
</div>
@endsection
