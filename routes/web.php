<?php

use App\Http\Controllers\HistoryOrderController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Order
Route::get('/order', [HistoryOrderController::class, 'index']);
Route::post('/order', [HistoryOrderController::class, 'store']);
